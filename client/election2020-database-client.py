import mysql.connector
import os
import sys
import time

# -----------------------------------------------------------------------------
# GLOBALS
# -----------------------------------------------------------------------------

db = None
connection = {
    'user': '',
    'host': '',
    'db': ''
}

# -----------------------------------------------------------------------------
# CLIENT AUTHENTICATION
# -----------------------------------------------------------------------------

def connect_to_mysql():
    global connection
    global db

    while True:
        host = input('MySQL Host: ')
        database = input('Database: ')
        userID = input('\nUserID: ')
        password = input('Password: ')

        try:
            db = mysql.connector.connect(
                host = host,
                user = userID,
                password = password,
                database = database
            )

            connection['user'] = userID
            connection['host'] = host
            connection['db'] = database

            print('\nSuccessfully logged into MySQL!\n')

            print('User: {}'.format(connection['user']))
            print('Host: {}'.format(connection['host']))
            print('Database: {}\n'.format(connection['db']))
            
            print(time.strftime('%c') + '\n')

            print('The 2020 Election CLI is a client which allows for the management of the 2020 Election database.')
            print('The client allows for various commands to retrive data and add notes to the database on MySQL.')

            break

        except:
            print('\nFailed to connect to MySQL.\n')
    
    return

# -----------------------------------------------------------------------------
# COMMANDS
# -----------------------------------------------------------------------------

# retrives list of all presidental candidates
def getCandidates():
    cursor = db.cursor()

    query = """
        SELECT * FROM Candidate 
    """
    cursor.execute(query)
    print('\n---------- Candidates in the 2020 Election (Name, Party) ----------')
    for row in cursor:
        try:
            print('{}, {}'.format(row[0], row[1]))
        except:
            continue

# retrives list all counties
def getCounties():
    cursor = db.cursor()

    query = """
        SELECT * FROM County
    """
    cursor.execute(query)
    print('\n---------- Counties (County, State) ----------')
    for row in cursor:
        try:
            print('{}, {}'.format(row[0], row[1]))
        except:
            continue

# retrives list of all states
def getStates():
    cursor = db.cursor()

    query = """
        SELECT * FROM State
    """
    cursor.execute(query)
    print('\n---------- State ----------')
    for row in cursor:
        try:
            print('{}'.format(row[0]))
        except:
            continue

# retrives county data, such as race percentages, unemployment, and COVID-19 statistics
def getCountyStatistics():
    cursor = db.cursor()
    
    print('\nWhich state + county would you like to view data for?')
    state = input('State: ')
    county = input('County: ')
    if state and county:
        query = """
            SELECT * FROM CountyStatistics
                WHERE county_name = %s 
                AND state_name = %s
        """

        params = (county.lower().title(), state.lower().title())
        cursor.execute(query, params)

        empty = True
        for row in cursor:
            empty = False
            print('\n---------- Statistics for {}, {} ----------'.format(county.lower().title(), state.lower().title()))
            try:
                print('Male Population: {}'.format(row[4]))
                print('Female Population: {}'.format(row[5]))
                print('Hispanic Percentage: {}'.format(row[6]))
                print('White Percentage: {}'.format(row[7]))
                print('Black Percentage: {}'.format(row[8]))
                print('Native Percentage: {}'.format(row[9]))
                print('Asian Percentage: {}'.format(row[10]))
                print('Pacific Percentage: {}'.format(row[11]))
                print('Average Income: {}'.format(row[12]))
                print('Unemployment Rate: {}'.format(row[13]))
                print('COVID-19 Case Count During Election: {}'.format(row[2]))
                print('COVID-19 Death Count During Election: {}'.format(row[3]))
            except:
                continue
        if empty:
            print('\nNo statistics were found for {}, {}'.format(county.lower().title(), state.lower().title()))

# retrives winning candidate in a county, total votes, and demographic data
def getCountyWinner():
    cursor = db.cursor()
    
    print('\nWhich state + county would you like to view the winning candidate for?')
    state = input('State: ')
    county = input('County: ')
    if state and county:
        query = """
            SELECT candidate_name, total_votes 
                FROM CountyPresidentCandidate 
                WHERE county_name = %s 
                AND state_name = %s
                AND total_votes = (
                    SELECT max(total_votes) 
                        FROM CountyPresidentCandidate 
                        WHERE county_name = %s 
                        AND state_name = %s
                )
        """
        params = (county.lower().title(), state.lower().title(), county.lower().title(), state.lower().title())
        cursor.execute(query, params)

        empty = True
        for row in cursor:
            empty = False
            print('\n---------- {}, {} ----------'.format(county.lower().title(), state.lower().title()))
            print('Winner: {}'.format(row[0]))
            print('Total Votes: {}'.format(row[1]))
        if empty:
            print('\nNo voting results were found for {}, {}'.format(county.lower().title(), state.lower().title()))
            return
        
        demo = input('\nWould you like to view statistics for this county? (Y/N): ')
        if demo.lower() == 'y':
            query = """
                SELECT * FROM CountyStatistics 
                    WHERE county_name = %s 
                    AND state_name = %s
            """
            params = (county.lower().title(), state.lower().title())
            cursor.execute(query, params)

            empty = True
            for row in cursor:
                empty = False
                print('\n---------- Statistics for {}, {} ----------'.format(county.lower().title(), state.lower().title()))
                try:
                    print('Male Population: {}'.format(row[4]))
                    print('Female Population: {}'.format(row[5]))
                    print('Hispanic Percentage: {}'.format(row[6]))
                    print('White Percentage: {}'.format(row[7]))
                    print('Black Percentage: {}'.format(row[8]))
                    print('Native Percentage: {}'.format(row[9]))
                    print('Asian Percentage: {}'.format(row[10]))
                    print('Pacific Percentage: {}'.format(row[11]))
                    print('Average Income: {}'.format(row[12]))
                    print('Unemployment Rate: {}'.format(row[13]))
                    print('COVID-19 Case Count During Election: {}'.format(row[2]))
                    print('COVID-19 Death Count During Election: {}'.format(row[3]))
                except:
                    continue
            if empty:
                print('\nNo statistics were found for {}, {}'.format(county.lower().title(), state.lower().title()))

# retrives voting results for all candidates in a county and demographic data
def getCountyVotingResults():
    cursor = db.cursor()
    
    print('\nWhich state + county would you like to view voting results for?')
    state = input('State: ')
    county = input('County: ')
    if state and county:
        query = """
            SELECT candidate_name, total_votes 
                FROM CountyPresidentCandidate 
                WHERE county_name = %s 
                AND state_name = %s
        """
        params = (county.lower().title(), state.lower().title())
        cursor.execute(query, params)

        empty = True
        print('\n---------- {}, {} ----------'.format(county.lower().title(), state.lower().title()))
        for row in cursor:
            empty = False
            print('Votes for {}: {}'.format(row[0], row[1]))
        
        if empty:
            print('\nNo voting results were found for {}, {}'.format(county.lower().title(), state.lower().title()))
            return
        
        demo = input('\nWould you like to view statistics for this county? (Y/N): ')
        if demo.lower() == 'y':
            query = """
                SELECT * FROM CountyStatistics 
                    WHERE county_name = %s 
                    AND state_name = %s
            """
            params = (county.lower().title(), state.lower().title())
            cursor.execute(query, params)

            empty = True
            for row in cursor:
                empty = False
                print('\n---------- Statistics for {}, {} ----------'.format(county.lower().title(), state.lower().title()))
                try:
                    print('Male Population: {}'.format(row[4]))
                    print('Female Population: {}'.format(row[5]))
                    print('Hispanic Percentage: {}'.format(row[6]))
                    print('White Percentage: {}'.format(row[7]))
                    print('Black Percentage: {}'.format(row[8]))
                    print('Native Percentage: {}'.format(row[9]))
                    print('Asian Percentage: {}'.format(row[10]))
                    print('Pacific Percentage: {}'.format(row[11]))
                    print('Average Income: {}'.format(row[12]))
                    print('Unemployment Rate: {}'.format(row[13]))
                    print('COVID-19 Case Count During Election: {}'.format(row[2]))
                    print('COVID-19 Death Count During Election: {}'.format(row[3]))
                except:
                    continue
                
            if empty:
                print('\nNo statistics were found for {}, {}'.format(county.lower().title(), state.lower().title()))
                
# retrives counties where a candidate won and average demographic data
def getWinningCounties():
    cursor = db.cursor()
    
    print('\nWhich candidate would you view the winning counties for?')
    candidate = input('Candidate Full Name: ')
    if candidate:
        query = """
            WITH
                CountyMaxVotes AS (
                    SELECT county_name, state_name, max(total_votes) AS max_votes 
                    FROM CountyPresidentCandidate 
                    GROUP BY county_name, state_name
            )
            SELECT county_name, state_name 
                FROM CountyPresidentCandidate INNER JOIN CountyMaxVotes USING (county_name, state_name) 
                WHERE candidate_name = %s and total_votes = max_votes
        """
        params = (candidate.lower().title(),)
        cursor.execute(query, params)

        empty = True
        print('\n---------- Winning Counties for {} ----------'.format(candidate.lower().title()))
        for row in cursor:
            empty = False
            try:
                print('{}, {}'.format(row[0], row[1]))
            except:
                continue
        
        if empty:
            print('\nNo county wins were found for {}'.format(candidate.lower().title()))
            return
        
        demo = input('\nWould you like to view the average statistics for the winning counties? (Y/N): ')
        if demo.lower() == 'y':
            query = """
                WITH
                    CountyMaxVotes AS (
                        SELECT county_name, state_name, max(total_votes) AS max_votes 
                        FROM CountyPresidentCandidate 
                        GROUP BY county_name, state_name
                    ),
                    WinningCounties AS (
                        SELECT county_name, state_name  
                        FROM CountyPresidentCandidate INNER JOIN CountyMaxVotes USING (county_name, state_name) 
                        WHERE candidate_name = %s AND total_votes = max_votes)
                SELECT
                    avg(covid_cases_count),
                    avg(covid_death_count),
                    avg(population_men),
                    avg(population_women),
                    avg(hispanic_percentage),
                    avg(white_percentage),
                    avg(black_percentage),
                    avg(native_percentage),
                    avg(asian_percentage),
                    avg(pacific_percentage),
                    avg(avg_income),
                    avg(unemployment_rate)
                FROM WinningCounties INNER JOIN CountyStatistics USING (county_name, state_name)
            """
            params = (candidate.lower().title(),)

            cursor.execute(query, params)
            for row in cursor:
                print('\n---------- Average Statistics for Winning Counties for {} ----------'.format(candidate.lower().title()))
                try:
                    print('Male Population: {}'.format(int(row[2])))
                    print('Female Population: {}'.format(int(row[3])))
                    print('Hispanic Percentage: {}'.format(row[4]))
                    print('White Percentage: {}'.format(row[5]))
                    print('Black Percentage: {}'.format(row[6]))
                    print('Native Percentage: {}'.format(row[7]))
                    print('Asian Percentage: {}'.format(row[8]))
                    print('Pacific Percentage: {}'.format(row[9]))
                    print('Average Income: {}'.format(row[10]))
                    print('Unemployment Rate: {}'.format(row[11]))
                    print('COVID-19 Case Count During Election: {}'.format(row[0]))
                    print('COVID-19 Death Count During Election: {}'.format(row[1]))
                except:
                    continue

# retrives highest polled candidate in a state
def getStatePollWinners():
    cursor = db.cursor()
    
    print('\nWhich state would you like to view polling results for?')
    state = input('State: ')
    if state:
        query = """
            WITH
                StateMaxPoll AS (
                    SELECT question_id, display_name, max(pct) AS max_pct 
                    FROM StatePoll
                    WHERE state_name = %s
                    GROUP BY question_id, display_name
            )
            SELECT display_name, candidate_name, pct
                FROM StatePoll INNER JOIN StateMaxPoll USING (question_id, display_name) 
                WHERE pct = max_pct
        """
        params = (state.lower().title(),)
        cursor.execute(query, params)

        empty = True
        for row in cursor:
            empty = False
            try:
                print('\n---------- {} Poll in {} ----------'.format(row[0], state.lower().title()))
                print('Highest Polled: {}'.format(row[1]))
                print('Percentage: {}'.format(row[2]))
            except:
                continue
        
        if empty:
            print('\nNo polls were found for the state of {}'.format(state.lower().title()))
            return

# retrives polling results for candidates in a state
def getStatePollingResults():
    cursor = db.cursor()
    
    print('\nWhich state would you like to view polling results for?')
    state = input('State: ')
    if state:
        query = """
            SELECT *
                FROM StatePoll
                WHERE state_name = %s
        """
        params = (state.lower().title(),)
        cursor.execute(query, params)

        empty = True
        for row in cursor:
            try:
                print('\n---------- {} Poll in {} ----------'.format(row[3], state.lower().title()))
                print('Candidate: {}'.format(row[1]))
                print('Percentage: {}'.format(row[4]))
            except:
                continue

        if empty:
            print('\nNo polls were found for the state of {}'.format(state.lower().title()))
            return

# retrives a note or annotation about a particular county
def getNoteAboutCounty():
    cursor = db.cursor()
    
    print('\nWhich state + county would you like to view a note about?')
    state = input('State: ')
    county = input('County: ')
    if state and county:
        query = """
            SELECT note FROM County
                WHERE county_name = %s 
                AND state_name = %s
        """
        params = (county.lower().title(), state.lower().title())

        try:
            cursor.execute(query, params)

            empty = True
            for row in cursor:
                if row[0] == None:
                    break

                empty = False
                try:
                    print('\n---------- Note for {}, {} ----------'.format(county.lower().title(), state.lower().title()))
                    print('{}'.format(row[0]))
                except:
                    continue
            
            if empty:
                print('\nNo note was found for {}, {}. Use addNoteAboutCounty to add a note.'.format(county.lower().title(), state.lower().title()))
                return

        except:
            print('\nNo note was found for {}, {}. Use addNoteAboutCounty to add a note.'.format(county.lower().title(), state.lower().title()))

# add a note or annotation about a particular county
def addNoteAboutCounty():
    cursor = db.cursor(buffered=True)
    
    print('\nWhich state + county would you like to make a note about?')
    state = input('State: ')
    county = input('County: ')
    if state and county:
        query = """
            SELECT note 
                FROM County
        """
        try:
            cursor.execute(query)
        except:
            query = """
                ALTER TABLE County
                    ADD note varchar(500) NULL
            """
            cursor.execute(query)
            db.commit()

        note = input('\nPlease enter your note you would like to make for {}, {} (max 500 chars): '.format(county.lower().title(), state.lower().title()))
        if note:
            query = """
                UPDATE County
                    SET note = %s 
                    WHERE county_name = %s 
                    AND state_name = %s
            """
            params = (note, county.lower().title(), state.lower().title())

            cursor.execute(query, params)
            db.commit()

            cursor = db.cursor()
            query = """
            SELECT note
                FROM County
                WHERE county_name = %s 
                AND state_name = %s
            """
            params = (county.lower().title(), state.lower().title())
            cursor.execute(query, params)

            empty = True
            for row in cursor:
                empty = False

            if empty:
                print('\nFailed to add note to {}, {}.'.format(county.lower().title(), state.lower().title()))
                return

            print('\nSuccess')

# retrives the number of tweets in a county with #biden and #trump or both. Also shows county election winner.
def getTweetCountInCounty():
    cursor = db.cursor()
    
    print('\nWhich state + county would you like to view the tweet counts for?')
    state = input('State: ')
    county = input('County: ')
    if state and county:
        # hashtag joebiden or biden
        query = """
            WITH CountyTweet AS (
                SELECT *
                    FROM Tweet INNER JOIN Location USING (lon, lat)
                    INNER JOIN CountyZipcode USING (zipcode)
                    WHERE county_name = %s
                    AND state_name = %s
            )
            SELECT count(tweet_id) AS tweet_count 
                FROM CountyTweet 
                WHERE hashtag_biden = b'1' 
                AND hashtag_trump = b'0'
        """
        params = (county.lower().title(), state.lower().title())
        cursor.execute(query, params)

        empty = True
        for row in cursor:
            empty = False
            print('\nNumber of #biden or #joebiden tweets: {}'.format(row[0]))
        
        if empty:
            print('\nNo #biden or #joebiden tweets were found in {}, {}'.format(county.lower().title(), state.lower().title()))
            return
        
        # hashtag donaldtrump or trump
        query = """
            WITH CountyTweet AS (
                SELECT *
                    FROM Tweet INNER JOIN Location USING (lon, lat)
                    INNER JOIN CountyZipcode USING (zipcode)
                    WHERE county_name = %s
                    AND state_name = %s
            )
            SELECT count(tweet_id) AS tweet_count 
                FROM CountyTweet 
                WHERE hashtag_biden = b'0' 
                AND hashtag_trump = b'1'
        """
        params = (county.lower().title(), state.lower().title())
        cursor.execute(query, params)

        empty = True
        for row in cursor:
            empty = False
            print('Number of #trump or #donaldtrump tweets: {}'.format(row[0]))
        
        if empty:
            print('\nNo #trump or #donaldtrump tweets were found in {}, {}'.format(county.lower().title(), state.lower().title()))
            return
        
        # hashtag both
        query = """
            WITH CountyTweet AS (
                SELECT *
                    FROM Tweet INNER JOIN Location USING (lon, lat)
                    INNER JOIN CountyZipcode USING (zipcode)
                    WHERE county_name = %s
                    AND state_name = %s
            )
            SELECT count(tweet_id) AS tweet_count 
                FROM CountyTweet WHERE 
                hashtag_biden = b'1' 
                AND hashtag_trump = b'1'
        """
        params = (county.lower().title(), state.lower().title())
        cursor.execute(query, params)

        empty = True
        for row in cursor:
            empty = False
            print('Number of #biden or #joebiden and #trump or #donaldtrump tweets: {}'.format(row[0]))
        
        if empty:
            print('\nNo #biden or #joebiden and #trump or #donaldtrump tweets were found in {}, {}'.format(county.lower().title(), state.lower().title()))
            return
        
        query = """
            SELECT candidate_name, total_votes 
                FROM CountyPresidentCandidate 
                WHERE county_name = %s 
                AND state_name = %s
                AND total_votes = (
                    SELECT max(total_votes) 
                        FROM CountyPresidentCandidate 
                        WHERE county_name = %s 
                        AND state_name = %s
                )
        """
        params = (county.lower().title(), state.lower().title(), county.lower().title(), state.lower().title())
        cursor.execute(query, params)

        empty = True
        for row in cursor:
            empty = False
            print('\nCounty election winner: {}'.format(row[0]))

        if empty:
            print('\nNo voting results were found for {}, {}'.format(county.lower().title(), state.lower().title()))
            return

# retrives total interaction (likes, retweets) of tweets in a county with #biden and #trump or both. Also shows county election winner.
def getTweetInteractionInCounty():
    cursor = db.cursor()
    
    print('\nWhich state + county would you like to view the tweet interactions for?')
    state = input('State: ')
    county = input('County: ')
    if state and county:
        # hashtag joebiden or biden
        query = """
            WITH CountyTweet AS (
                SELECT *
                    FROM Tweet INNER JOIN Location USING (lon, lat)
                    INNER JOIN CountyZipcode USING (zipcode)
                    WHERE county_name = %s
                    AND state_name = %s
            )
            SELECT sum(likes)+sum(retweet_count) AS tweet_interaction 
                FROM CountyTweet 
                WHERE hashtag_biden = b'1' 
                AND hashtag_trump = b'0'
        """
        params = (county.lower().title(), state.lower().title())
        cursor.execute(query, params)

        empty = True
        for row in cursor:
            empty = False
            print('\nNumber of #biden or #joebiden tweet interactions (likes + retweets): {}'.format(row[0]))
        
        if empty:
            print('\nNo #biden or #joebiden tweets were found in {}, {}'.format(county.lower().title(), state.lower().title()))
            return
        
        # hashtag donaldtrump or trump
        query = """
            WITH CountyTweet AS (
                SELECT *
                    FROM Tweet INNER JOIN Location USING (lon, lat)
                    INNER JOIN CountyZipcode USING (zipcode)
                    WHERE county_name = %s
                    AND state_name = %s
            )
            SELECT sum(likes)+sum(retweet_count) AS tweet_interaction 
                FROM CountyTweet 
                WHERE hashtag_biden = b'0' 
                AND hashtag_trump = b'1'
        """
        params = (county.lower().title(), state.lower().title())
        cursor.execute(query, params)

        empty = True
        for row in cursor:
            empty = False
            print('Number of #trump or #donaldtrump tweet interactions (likes + retweets): {}'.format(row[0]))
        
        if empty:
            print('\nNo #trump or #donaldtrump tweets were found in {}, {}'.format(county.lower().title(), state.lower().title()))
            return
        
        # hashtag both
        query = """
            WITH CountyTweet AS (
                SELECT *
                    FROM Tweet INNER JOIN Location USING (lon, lat)
                    INNER JOIN CountyZipcode USING (zipcode)
                    WHERE county_name = %s
                    AND state_name = %s
            )
            SELECT sum(likes)+sum(retweet_count) AS tweet_interaction 
                FROM CountyTweet 
                WHERE hashtag_biden = b'1' 
                AND hashtag_trump = b'1'
        """
        params = (county.lower().title(), state.lower().title())
        cursor.execute(query, params)

        empty = True
        for row in cursor:
            empty = False
            print('Number of #biden or #joebiden and #trump or #donaldtrump tweet interactions (likes + retweets): {}'.format(row[0]))
        
        if empty:
            print('\nNo #biden or #joebiden and #trump or #donaldtrump tweets were found in {}, {}'.format(county.lower().title(), state.lower().title()))
            return
        
        query = """
            SELECT candidate_name, total_votes 
                FROM CountyPresidentCandidate 
                WHERE county_name = %s 
                AND state_name = %s
                AND total_votes = (
                    SELECT max(total_votes) 
                        FROM CountyPresidentCandidate 
                        WHERE county_name = %s 
                        AND state_name = %s
                )
        """
        params = (county.lower().title(), state.lower().title(), county.lower().title(), state.lower().title())
        cursor.execute(query, params)

        empty = True
        for row in cursor:
            empty = False
            print('\nCounty election winner: {}'.format(row[0]))

        if empty:
            print('\nNo voting results were found for {}, {}'.format(county.lower().title(), state.lower().title()))
            return

# retrives number of tweets from iPhone and Android with #biden and #trump or both
def getTweetSources():
    cursor = db.cursor()
    
    # hashtag joebiden or biden from iPhone
    query = """
        SELECT count(tweet_id) as tweet_count 
            FROM Tweet 
            WHERE hashtag_biden = b'1' 
            AND hashtag_trump = b'0'
            AND source = 'Twitter for iPhone'
    """
    cursor.execute(query)

    empty = True
    for row in cursor:
        empty = False
        print('\nNumber of #biden or #joebiden tweets from iPhone: {}'.format(row[0]))
    
    if empty:
        print('\nNo #biden or #joebiden tweets were found from iPhone')
        return

    # hashtag joebiden or biden from Android
    query = """
        SELECT count(tweet_id) as tweet_count 
            FROM Tweet 
            WHERE hashtag_biden = b'1' 
            AND hashtag_trump = b'0'
            AND source = 'Twitter for Android'
    """
    cursor.execute(query)

    empty = True
    for row in cursor:
        empty = False
        print('Number of #biden or #joebiden tweets from Android: {}'.format(row[0]))
    
    if empty:
        print('\nNo #biden or #joebiden tweets were found from Android')
        return
    
    # hashtag donaldtrump or trump from iPhone
    query = """
        SELECT count(tweet_id) as tweet_count 
            FROM Tweet 
            WHERE hashtag_biden = b'0' 
            AND hashtag_trump = b'1'
            AND source = 'Twitter for iPhone'
    """
    cursor.execute(query)

    empty = True
    for row in cursor:
        empty = False
        print('\nNumber of #trump or #donaldtrump tweets from iPhone: {}'.format(row[0]))
    
    if empty:
        print('\nNo #trump or #donaldtrump tweets were found from iPhone')
        return
    
    # hashtag donaldtrump or trump from Android
    query = """
        SELECT count(tweet_id) as tweet_count 
            FROM Tweet 
            WHERE hashtag_biden = b'0' 
            AND hashtag_trump = b'1'
            AND source = 'Twitter for Android'
    """
    cursor.execute(query)

    empty = True
    for row in cursor:
        empty = False
        print('Number of #trump or #donaldtrump tweets from Android: {}'.format(row[0]))
    
    if empty:
        print('\nNo #trump or #donaldtrump tweets were found from Android')
        return

    # hashtag biden or joebiden and trump or donaldtrump from iPhone
    query = """
        SELECT count(tweet_id) as tweet_count 
            FROM Tweet 
            WHERE hashtag_biden = b'1' 
            AND hashtag_trump = b'1'
            AND source = 'Twitter for iPhone'
    """
    cursor.execute(query)

    empty = True
    for row in cursor:
        empty = False
        print('\nNumber of #biden or #joebiden and #trump or #donaldtrump tweets from iPhone: {}'.format(row[0]))
    
    if empty:
        print('\nNo #biden or #joebiden and #trump or #donaldtrump tweets were found from iPhone')
        return
    
    # hashtag biden or joebiden and trump or donaldtrump from Android
    query = """
        SELECT count(tweet_id) as tweet_count 
            FROM Tweet 
            WHERE hashtag_biden = b'1' 
            AND hashtag_trump = b'1'
            AND source = 'Twitter for Android'
    """
    cursor.execute(query)

    empty = True
    for row in cursor:
        empty = False
        print('Number of #biden or #joebiden and #trump or #donaldtrump tweets from Android: {}'.format(row[0]))
    
    if empty:
        print('\nNo #biden or #joebiden and #trump or #donaldtrump tweets were found from Android')
        return

# retrives top ten most popular tweets from the 2020 Election based on interation (likes, retweets)
def getMostPopularTweets():
    cursor = db.cursor()
    
    query = """
        SELECT user_name, tweet, likes, retweet_count
            FROM Tweet
            ORDER BY likes+retweet_count DESC
            LIMIT 10
    """
    cursor.execute(query)

    empty = True
    for row in cursor:
        empty = False
        try:
            print('\n---------- Tweet by {} ----------'.format(row[0]))
            print('Likes: {}'.format(row[2]))
            print('Retweets: {}'.format(row[3]))
            print('Tweet: {}'.format(row[1]))
        except:
            continue
        
    if empty:
        print('\nNo tweets were found.')
        return

# -----------------------------------------------------------------------------
# CLIENT PROMPT
# -----------------------------------------------------------------------------

def welcome():
    print('+------------------------------------------------------------------+')
    print('|           Welcome to the 2020 Election MySQL Database Client     |')
    print('+------------------------------------------------------------------+\n')

    print("To login to MySQL, use 'login'")
    print("To quit, use 'exit'\n")

    while True:
        action = input('2020ElectionCLI> ')
        if action == 'login':
            connect_to_mysql()
            break
        elif action == 'exit':
            quit()
            break
    
    return

def mainPrompt():
    print('\n------------------------------------------------------------------------------------------------------------')
    print('Avaliable commands:\n')
    print('     getCandidates                   Retrives a list of all presidental candidates.')
    print('     getCounties                     Retrives a list of all counties.')
    print('     getStates                       Retrives a list of all states.\n')
    print('     getCountyStatistics             Retrives county data, such as percentages by race, unemployment, and COVID-19 statistics.')
    print('     getCountyWinner                 Retrives winning candidate in a county, total votes, and demographic data + statistics.')
    print('     getCountyVotingResults          Retrives voting results for all candidates in a county and demographic data + statistics.')
    print('     getWinningCounties              Retrives counties where a candidate won and average demographic data + statistics.\n')
    print('     getStatePollWinners             Retrives highest polled candidate in a state for various polls. Not all states available.')
    print('     getStatePollingResults          Retrives polling results for candidates in a state for various polls. Not all states available.\n')
    print('     getNoteAboutCounty              Retrives a note or annotation about a particular county.')
    print('     addNoteAboutCounty              Add a note or annotation about a particular county.\n')
    print('     getTweetCountInCounty           Retrives the number of tweets in a county with #biden and #trump or both. Also shows county election winner.')
    print('     getTweetInteractionInCounty     Retrives total interaction (likes, retweets) of tweets in a county with #biden and #trump or both. Also shows county election winner.')
    print('     getTweetSources                 Retrives number of tweets from iPhone and Android with #biden and #trump or both.')
    print('     getTopTweets                    Retrives top ten most popular tweets from the 2020 Election based on interation (likes, retweets).\n')
    print('     exit                            Quit the client\n')
    print('------------------------------------------------------------------------------------------------------------')

def main():
    global connection

    mainPrompt()
    try:
        command = input('{}@2020ElectionCLI> '.format(connection['user']))
        if command == 'getCandidates':
            getCandidates()
        elif command == 'getCounties':
            getCounties()
        elif command == 'getStates':
            getStates()
        elif command == 'getCountyStatistics':
            getCountyStatistics()
        elif command == 'getCountyWinner':
            getCountyWinner()
        elif command == 'getCountyVotingResults':
            getCountyVotingResults()
        elif command == 'getWinningCounties':
            getWinningCounties()
        elif command == 'getStatePollWinners':
            getStatePollWinners()
        elif command == 'getStatePollingResults':
            getStatePollingResults()
        elif command == 'getNoteAboutCounty':
            getNoteAboutCounty()
        elif command == 'addNoteAboutCounty':
            addNoteAboutCounty()
        elif command == 'addNoteAboutVotingResults':
            addNoteAboutVotingResults()
        elif command == 'getTweetCountInCounty':
            getTweetCountInCounty()
        elif command == 'getTweetInteractionInCounty':
            getTweetInteractionInCounty()
        elif command == 'getTweetSources':
            getTweetSources()
        elif command == 'getMostPopularTweets':
            getMostPopularTweets()
        elif command == 'exit':
            quit()

        main()
    except KeyboardInterrupt:
        sys.exit()

if __name__ == '__main__':
    os.system('clear')

    welcome()
    main()

