# 2020 Election Databaser Client Test Cases

The following three tests are used to verify that the client is
running correctly. These tests should be run after running the SQL 
script for the database setup. While the tests are not exhausive, they
cover the most important aspects of the client: correct results and 
error handling.

## Test 1
#### Purpose
The purpose of this test is to ensure that the client successfully connects to MySQL.
#### Input
The input is using the command login and entering a valid MySQL host, database, username and password.

    To login to MySQL, use 'login'
    To quit, use 'exit'

    2020ElectionCLI> login

    Host: <host_name>
    Database: <database_name>

    UserID: <userID>
    Password: <password>
#### Expected Output
The client should successfully connect to MySQL and show the following prompt:

    Successfully logged into MySQL!

    User: <userID>
    Host: <host_name>
    Database: <database_name>

    <current_date>

    The 2020 Election CLI is a client which allows for the management of the 2020 Election database.
    The client allows for various commands to retrive data and add notes to the database on MySQL.

    ------------------------------------------------------------------------------------------------------------
    Avaliable commands:

     getCandidates                   Retrives a list of all presidental candidates.
     getCounties                     Retrives a list of all counties.
     getStates                       Retrives a list of all states.

     getCountyStatistics             Retrives county data, such as percentages by race, unemployment, and COVID-19 statistics.
     getCountyWinner                 Retrives winning candidate in a county, total votes, and demographic data + statistics.
     getCountyVotingResults          Retrives voting results for all candidates in a county and demographic data + statistics.
     getWinningCounties              Retrives counties where a candidate won and average demographic data + statistics.

     getStatePollWinners             Retrives highest polled candidate in a state for various polls. Not all states available.
     getStatePollingResults          Retrives polling results for candidates in a state for various polls. Not all states available.

     getNoteAboutCounty              Retrives a note or annotation about a particular county.
     addNoteAboutCounty              Add a note or annotation about a particular county.

     getTweetCountInCounty           Retrives the number of tweets in a county with #biden and #trump or both. Also shows county election winner.
     getTweetInteractionInCounty     Retrives total interaction (likes, retweets) of tweets in a county with #biden and #trump or both. Also shows county election winner.
     getTweetSources                 Retrives number of tweets from iPhone and Android with #biden and #trump or both.
     getTopTweets                    Retrives top ten most popular tweets from the 2020 Election based on interation (likes, retweets).

     exit                            Quit the client
    ------------------------------------------------------------------------------------------------------------

## Test 2
#### Purpose
The purpose of this test is to ensure that data can be retrived using a command.
#### Input
The input is running the getCountyStatistics command and entering the state of Vermont
and county of Addison:
    
    <userID>@2020ElectionCLI> getCountyStatistics

    Which state + county would you like to view data for?
    State: Vermont
    County: Addison
#### Expected Output
The expected output is shown below:

    ---------- Statistics for Addison, Vermont ----------
    Male Population: 18214
    Female Population: 18611
    Hispanic Percentage: 2.20
    White Percentage: 93.00
    Black Percentage: 0.90
    Native Percentage: 0.30
    Asian Percentage: 1.90
    Pacific Percentage: 0.00
    Average Income: 61875
    Unemployment Rate: 4.40
    COVID-19 Case Count During Election: 124
    COVID-19 Death Count During Election: 2

## Test 3
#### Purpose
The purpose of this test is to ensure that data can be added using a command.
#### Input
The input is running the addNoteAboutCounty command and entering the state of Vermont
and county of Addison. Any note can then be added as seen below:

    <userID>@2020ElectionCLI> addNoteAboutCounty

    Which state + county would you like to make a note about?
    State: Vermont
    County: Addison

    Please enter your note you would like to make for Addison, Vermont (max 500 chars): This is a note.

    Success

#### Expected Output
The expected output is that the note was saved with the county. To test this,
run the getNoteAboutCounty command and enter the state of Vermont and county
of Addison. The same note should be returned as seen below:

    <userID>@2020ElectionCLI> getNoteAboutCounty

    Which state + county would you like to view a note about?
    State: Vermont
    County: Addison

    ---------- Note for Addison, Vermont ----------
    This is a note.

## Test 4
#### Purpose
The purpose of this test is to ensure that invalid inputs are handled.
#### Input
The input is running the getWinningCounty command and entering a fake state name
and a fake county name:

    <userID>@2020ElectionCLI> getCountyWinner

    Which state + county would you like to view the winning candidate for?
    State: Fake State
    County: Fake County

#### Expected Output
The expected output is that the invalid state and county names are rejected:

    No voting results were found for Fake County, Fake State
