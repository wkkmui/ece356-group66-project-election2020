-- US Election 2020
-- ECE 356 - Project - Group 66
-- Written By: Malik Salvosa, Sarthak Tamboli, William Kenna Kawing Mui

DROP TABLE IF EXISTS Tweet;
DROP TABLE IF EXISTS CountyStatistics;
DROP TABLE IF EXISTS StatePoll;
DROP TABLE IF EXISTS CountyPresidentCandidate;
DROP TABLE IF EXISTS Candidate;
DROP TABLE IF EXISTS Location;
DROP TABLE IF EXISTS CountyZipcode;
DROP TABLE IF EXISTS County;
DROP TABLE IF EXISTS State;


-- ****************************************************************************
-- State Table
-- ****************************************************************************
SELECT '----------------------------------------------------------------' AS '';

-- create State table
SELECT 'Create State table' AS '';
CREATE TABLE State (
    state_name varchar(20),

    PRIMARY KEY (state_name)
);

-- load data from president_state.csv into State
SELECT 'Load data from president_state.csv into State table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/05-2020-Election/president_state.csv'
IGNORE INTO TABLE State
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(state_name, @col2);

-- ****************************************************************************
-- County Table
-- ****************************************************************************
SELECT '----------------------------------------------------------------' AS '';

-- create County table
SELECT 'Create County table' AS '';
CREATE TABLE County (
    county_name varchar(50),
    state_name varchar(20),

    PRIMARY KEY (county_name, state_name),
    FOREIGN KEY (state_name) REFERENCES State (state_name)
        ON UPDATE CASCADE 
        ON DELETE CASCADE
);

-- load data from president_county.csv into County
SELECT 'Load data from president_county.csv into County table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/05-2020-Election/president_county.csv'
IGNORE INTO TABLE County
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(state_name, county_name, @col3, @col4, @col5);

-- ****************************************************************************
-- CountyStatistics Table
-- ****************************************************************************
SELECT '----------------------------------------------------------------' AS '';

-- create CountyStatisticsRaw table
SELECT 'Create temporary table' AS '';
CREATE TABLE CountyStatisticsRaw (
    county_name varchar(50),
    state_name varchar(20),
    covid_cases_count int,
    covid_death_count int,
    population_men int,
    population_women int,
    hispanic_percentage decimal(5, 2),
    white_percentage decimal(5, 2),
    black_percentage decimal(5, 2),
    native_percentage decimal(5, 2),
    asian_percentage decimal(5, 2),
    pacific_percentage decimal(5, 2),
    avg_income int,
    unemployment_rate decimal(4, 2),

    PRIMARY KEY (county_name, state_name),
    CHECK (population_men > 0),
    CHECK (population_women > 0)
);

-- load data from county_statistics.csv into CountyStatisticsRaw temporary table
SELECT 'Load data from county_statistics.csv into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/05-2020-Election/county_statistics.csv'
IGNORE INTO TABLE CountyStatisticsRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(@dummy, county_name, state_name, @dummy, @dummy, @dummy, @dummy, @dummy,
@dummy, @dummy, @dummy, @dummy, @dummy,
@dummy, @dummy, covid_cases_count, covid_death_count, @dummy, population_men, population_women,
hispanic_percentage, white_percentage, black_percentage, native_percentage, asian_percentage, pacific_percentage, 
@dummy, avg_income, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy,
@dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, unemployment_rate);

-- update state_name in CountyStatisticsRaw from abbreviations to full name
SELECT 'Update state from abbreviation to full name' AS '';
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'AL', 'Alabama');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'AK', 'Alaska');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'AZ', 'Arizona');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'AR', 'Arkansas');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'CA', 'California');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'CO', 'Colorado');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'CT', 'Connecticut	');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'DE', 'Delaware');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'DC', 'District of Columbia');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'FL', 'Florida');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'GA', 'Georgia');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'HI', 'Hawaii');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'ID', 'Idaho');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'IL', 'Illinois');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'IN', 'Indiana');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'IA', 'Iowa');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'KS', 'Kansas');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'KY', 'Kentucky');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'LA', 'Louisiana');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'ME', 'Maine');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'MD', 'Maryland');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'MA', 'Massachusetts');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'MI', 'Michigan');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'MN', 'Minnesota');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'MS', 'Mississippi');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'MO', 'Missouri');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'MT', 'Montana');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'NE', 'Nebraska');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'NV', 'Nevada');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'NH', 'New Hampshire');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'NJ', 'New Jersey');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'NM', 'New Mexico');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'NY', 'New York');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'NC', 'North Carolina');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'ND', 'North Dakota');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'OH', 'Ohio');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'OK', 'Oklahoma');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'OR', 'Oregon');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'PA', 'Pennsylvania');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'RI', 'Rhode Island');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'SC', 'South Carolina');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'SD', 'South Dakota');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'TN', 'Tennessee');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'TX', 'Texas');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'UT', 'Utah');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'VT', 'Vermont');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'VA', 'Virginia');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'WA', 'Washington');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'WV', 'West Virginia');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'WI', 'Wisconsin');
UPDATE CountyStatisticsRaw SET state_name = REPLACE(state_name, 'WY', 'Wyoming');

-- create CountyStatistics table
SELECT 'Create CountyStatistics table' AS '';
CREATE TABLE CountyStatistics (
    county_name varchar(50),
    state_name varchar(20),
    covid_cases_count int,
    covid_death_count int,
    population_men int,
    population_women int,
    hispanic_percentage decimal(5, 2),
    white_percentage decimal(5, 2),
    black_percentage decimal(5, 2),
    native_percentage decimal(5, 2),
    asian_percentage decimal(5, 2),
    pacific_percentage decimal(5, 2),
    avg_income int,
    unemployment_rate decimal(5, 2),

    PRIMARY KEY (county_name, state_name),
    FOREIGN KEY (county_name, state_name) REFERENCES County (county_name, state_name)
        ON UPDATE CASCADE 
        ON DELETE CASCADE
);

-- insert data from CountyStatisticsRaw table into CountyStatistics table
SELECT 'Insert data from temporary table into CountyStatistics table' AS '';
INSERT INTO CountyStatistics
    SELECT 
        county_name,
        state_name,
        covid_cases_count,
        covid_death_count,
        population_men,
        population_women,
        hispanic_percentage,
        white_percentage,
        black_percentage ,
        native_percentage,
        asian_percentage,
        pacific_percentage,
        avg_income,
        unemployment_rate
    FROM CountyStatisticsRaw INNER JOIN County using (county_name, state_name);

DROP TABLE CountyStatisticsRaw;

-- ****************************************************************************
-- CountyZipcodeRaw Table
-- ****************************************************************************
SELECT '----------------------------------------------------------------' AS '';

-- create temporary CountyZipcodeRaw table
-- temporary table purpose: fixing state_name, ensuring foreign keys have same value
SELECT 'Create temporary table' AS '';
CREATE TABLE CountyZipcodeRaw (
    zipcode int,
    county_name varchar(50),
    state_name varchar(20),

    PRIMARY KEY (zipcode)
);

-- load data from ZIP-COUNTY-FIPS_2017-06.csv into CountyZipcodeRaw
SELECT 'Load data from ZIP-COUNTY-FIPS_2017-06.csv.csv into tempoary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/05-2020-Election/ZIP-COUNTY-FIPS_2017-06.csv'
IGNORE INTO TABLE CountyZipcodeRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(zipcode, county_name, state_name, @col4, @col5);

-- update state_name in CountyZipcodeRaw from abbreviations to full name
SELECT 'Update state from abbreviation to full name' AS '';
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'AL', 'Alabama');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'AK', 'Alaska');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'AZ', 'Arizona');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'AR', 'Arkansas');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'CA', 'California');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'CO', 'Colorado');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'CT', 'Connecticut	');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'DE', 'Delaware');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'DC', 'District of Columbia');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'FL', 'Florida');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'GA', 'Georgia');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'HI', 'Hawaii');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'ID', 'Idaho');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'IL', 'Illinois');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'IN', 'Indiana');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'IA', 'Iowa');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'KS', 'Kansas');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'KY', 'Kentucky');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'LA', 'Louisiana');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'ME', 'Maine');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'MD', 'Maryland');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'MA', 'Massachusetts');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'MI', 'Michigan');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'MN', 'Minnesota');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'MS', 'Mississippi');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'MO', 'Missouri');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'MT', 'Montana');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'NE', 'Nebraska');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'NV', 'Nevada');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'NH', 'New Hampshire');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'NJ', 'New Jersey');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'NM', 'New Mexico');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'NY', 'New York');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'NC', 'North Carolina');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'ND', 'North Dakota');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'OH', 'Ohio');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'OK', 'Oklahoma');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'OR', 'Oregon');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'PA', 'Pennsylvania');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'RI', 'Rhode Island');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'SC', 'South Carolina');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'SD', 'South Dakota');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'TN', 'Tennessee');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'TX', 'Texas');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'UT', 'Utah');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'VT', 'Vermont');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'VA', 'Virginia');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'WA', 'Washington');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'WV', 'West Virginia');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'WI', 'Wisconsin');
UPDATE CountyZipcodeRaw SET state_name = REPLACE(state_name, 'WY', 'Wyoming');

-- create CountyZipcode table
SELECT 'Create CountyZipcode table' AS '';
CREATE TABLE CountyZipcode (
    zipcode int,
    county_name varchar(50),
    state_name varchar(20),

    PRIMARY KEY (zipcode),
    FOREIGN KEY (county_name, state_name) REFERENCES County (county_name, state_name)
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

-- insert data from temporary table into CountyZipcode table
SELECT 'Insert data from temporary table into CountyZipcode table' AS '';
INSERT INTO CountyZipcode (zipcode, county_name, state_name)
    SELECT zipcode, county_name, state_name
    FROM CountyZipcodeRaw INNER JOIN County using (county_name, state_name);

-- drop CountyzipcodeRaw
SELECT 'Drop temporary table' AS '';
DROP TABLE CountyZipcodeRaw;


-- ****************************************************************************
-- Location Table
-- ****************************************************************************
SELECT '----------------------------------------------------------------' AS '';

-- create temporary LonLatRaw table
-- temporary table purpose: retrieve the relevant lon/lat values which will be used from tweets
SELECT 'Create temporary table' AS '';
CREATE TABLE LonLatRaw (
    lon decimal(5,2),
    lat decimal(5,2),

    PRIMARY KEY (lon, lat)
);

-- load data from hashtag_joebiden.csv and hashtag_donaldtrump.csv into LonLatRaw
SELECT 'Load data from hashtag_joebiden.csv and hashtag_donaldtrump.csv into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/05-2020-Election/hashtag_joebiden.csv'
IGNORE INTO TABLE LonLatRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(@created_at,@tweet_id,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,
@dummy,@dummy,@dummy,lat,lon,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy);
LOAD DATA INFILE '/var/lib/mysql-files/05-2020-Election/hashtag_donaldtrump.csv'
IGNORE INTO TABLE LonLatRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(@created_at,@tweet_id,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,
@dummy,@dummy,@dummy,lat,lon,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy);

-- create temporary LocationRaw table
-- temporary table purpose: retrieve the all lon/lat values from OpenAddresses to map to zipcode
SELECT 'Create temporary table' AS '';
CREATE TABLE LocationRaw (
    lon decimal(5,2),
    lat decimal(5,2),
    zipcode int NOT NULL,

    PRIMARY KEY (lat, lon),
    CHECK (zipcode != 0)
);

-- load data from OpenAddresses state csv files into LocationRaw
SELECT 'Begin loading data from OpenAddresses csv files into temporary table. This may take a while.' AS '';
SELECT 'Load data from al.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/al.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from ak.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/ak.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from az.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/az.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from ar.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/ar.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from ca.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/ca.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from co.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/co.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from ct.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/ct.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from de.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/de.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from dc.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/dc.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from fl.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/fl.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from ga.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/ga.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from hi.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/hi.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from id.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/id.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from il.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/il.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from in.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/in.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from ia.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/ia.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from ks.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/ks.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from ky.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/ky.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from la.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/la.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from me.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/me.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from md.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/md.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from ma.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/ma.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from mi.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/mi.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from mn.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/mn.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from ms.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/ms.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from mo.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/mo.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from mt.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/mt.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from ne.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/ne.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from nv.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/nv.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from nh.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/nh.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from nj.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/nj.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from nm.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/nm.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from ny.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/ny.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from nc.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/nc.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from nd.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/nd.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from oh.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/oh.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from ok.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/ok.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from or.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/or.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from pa.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/pa.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from ri.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/ri.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from sc.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/sc.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from sd.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/sd.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from tn.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/tn.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from tx.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/tx.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from ut.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/ut.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from vt.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/vt.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from va.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/va.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from wa.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/wa.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from wv.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/wv.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from wi.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/wi.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);
SELECT 'Load data from wy.csv files into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/USAddresses/wy.csv'
IGNORE INTO TABLE LocationRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(lon, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, zipcode, @dummy, @dummy);

-- create Location table
SELECT 'Create Location table' AS '';
CREATE TABLE Location (
    lon decimal(5,2),
    lat decimal(5,2),
    zipcode int NOT NULL,

    PRIMARY KEY (lat, lon),
    FOREIGN KEY (zipcode) REFERENCES CountyZipcode (zipcode)
        ON UPDATE CASCADE 
        ON DELETE CASCADE
);

-- insert data from temporary table into Location table 
SELECT 'Insert data from temporary table into Location table' AS '';
INSERT INTO Location (lon, lat, zipcode)
    SELECT lon, lat, zipcode
    FROM LocationRaw INNER JOIN LonLatRaw using (lon, lat) 
        INNER JOIN CountyZipcode using (zipcode);

-- drop LonLatRaw
SELECT 'Drop temporary table' AS '';
DROP TABLE LonLatRaw;
-- drop LocationRaw
SELECT 'Drop temporary table' AS '';
DROP TABLE LocationRaw;


-- ****************************************************************************
-- Candidate Table
-- ****************************************************************************
SELECT '----------------------------------------------------------------' AS '';

-- create Candidate table
SELECT 'Create Candidate table' AS '';
CREATE TABLE Candidate(
    candidate_name varchar (50), -- 15,20 charactersfor first,last names respectively (added extra characters for middle name and spacing)
    party varchar (15),

    PRIMARY KEY (candidate_name)
);

-- load data from president_county_candidate.csv into Candidate table
SELECT 'Load data from president_county_candidate.csv into Candidate table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/05-2020-Election/president_county_candidate.csv'
IGNORE INTO TABLE Candidate
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(@col1, @col2, candidate_name, party, @col5, @col6);


-- ****************************************************************************
-- StatePoll Table
-- ****************************************************************************
SELECT '----------------------------------------------------------------' AS '';

-- create StatePollRaw temporary table
SELECT 'Create temporary table' AS '';
CREATE TABLE StatePollRaw (
    question_id decimal(6),
    candidate_name varchar(50),
    state_name varchar(20),
    display_name varchar(80),
    pct decimal(5,2),

    PRIMARY KEY (question_id, candidate_name),
    CHECK (question_id >= 136466) -- start_date value corrupt, any question with id >= 136466 is after Oct 20, 2020
);

-- load data from trump_biden_polls.csv into StatePollRaw
SELECT 'Load data from trump_biden_polls.csv into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/05-2020-Election/trump_biden_polls.csv'
IGNORE INTO TABLE StatePollRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(question_id,@dummy,@dummy,state_name,@dummy,@dummy,@dummy,@dummy,display_name,
@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,@start_date,@end_date,@dummy,@dummy,
@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy,
@dummy,@dummy,candidate_name,@dummy,pct);

UPDATE StatePollRaw SET candidate_name = REPLACE(candidate_name, 'Joseph R. Biden Jr.', 'Joe Biden');

-- create StatePoll table
SELECT 'Create StatePoll table' AS '';
CREATE TABLE StatePoll (
    question_id decimal(6),
    candidate_name varchar(50),
    state_name varchar(20),
    display_name varchar(80),
    pct decimal(5,2),

    PRIMARY KEY (question_id, candidate_name),
    FOREIGN KEY (candidate_name) REFERENCES Candidate (candidate_name)
        ON UPDATE CASCADE 
        ON DELETE CASCADE,
    FOREIGN KEY (state_name) REFERENCES State (state_name)
        ON UPDATE CASCADE 
        ON DELETE CASCADE
);

-- insert data from temporary table into StatePoll table
SELECT 'Insert data from temporary table into StatePoll table' AS '';
INSERT INTO StatePoll
    SELECT
        question_id,
        candidate_name,
        state_name,
        display_name,
        pct
    FROM StatePollRaw INNER JOIN Candidate USING (candidate_name)
        INNER JOIN State USING (state_name);

-- drop StatePollRaw
SELECT 'Drop temporary table' AS '';
DROP TABLE StatePollRaw;


-- ****************************************************************************
-- CountyPresidentCandidate Table
-- ****************************************************************************
SELECT '----------------------------------------------------------------' AS '';

-- create CountyPresidentCandidate table
SELECT 'Create CountyPresidentCandidate table' AS '';
CREATE TABLE CountyPresidentCandidate(
    county_name varchar(50),
    state_name varchar(20),
    candidate_name varchar(50),
    total_votes int,

    PRIMARY KEY (county_name, state_name, candidate_name),
    FOREIGN KEY (county_name, state_name) REFERENCES County (county_name, state_name)
        ON UPDATE CASCADE 
        ON DELETE CASCADE,
    FOREIGN KEY (candidate_name) REFERENCES Candidate (candidate_name)
        ON UPDATE CASCADE 
        ON DELETE CASCADE
);

-- load data from president_county_candidate.csv into CountyPresidentCandidate table
SELECT 'Load data from president_county_candidate.csv into CountyPresidentCandidate table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/05-2020-Election/president_county_candidate.csv'
IGNORE INTO TABLE CountyPresidentCandidate
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(state_name, county_name, candidate_name, @col4, total_votes, @col6);


-- ****************************************************************************
-- Tweet Table
-- ****************************************************************************
SELECT '----------------------------------------------------------------' AS '';

-- create TweetBidenRaw table
SELECT 'Create temporary table' AS '';
CREATE TABLE TweetBidenRaw (
    tweet_id decimal(65, 30),
    hashtag_biden bit DEFAULT b'0',
    hashtag_trump bit DEFAULT b'0',
    created_at datetime,
    tweet varchar(280),
    likes int,
    retweet_count int,
    source varchar(64),
    user_id int,
    user_name varchar(15),
    user_screen_name varchar(50),
    user_description varchar(160),
    user_followers_count int,
    user_location varchar(280),
    lat decimal(5,2),
    lon decimal(5,2),

    PRIMARY KEY (tweet_id)
);

-- create TweetTrumpRaw table
SELECT 'Create temporary table' AS '';
CREATE TABLE TweetTrumpRaw (
    tweet_id decimal(65, 30),
    hashtag_biden bit DEFAULT b'0',
    hashtag_trump bit DEFAULT b'0',
    created_at datetime,
    tweet varchar(280),
    likes int,
    retweet_count int,
    source varchar(64),
    user_id int,
    user_name varchar(15),
    user_screen_name varchar(50),
    user_description varchar(160),
    user_followers_count int,
    user_location varchar(280),
    lat decimal(5,2),
    lon decimal(5,2),

    PRIMARY KEY (tweet_id)
);

-- create TweetBidenTrumpRaw table
SELECT 'Create temporary table' AS '';
CREATE TABLE TweetBidenTrumpRaw (
    tweet_id decimal(65, 30),
    hashtag_biden bit DEFAULT b'0',
    hashtag_trump bit DEFAULT b'0',
    created_at datetime,
    tweet varchar(280),
    likes int,
    retweet_count int,
    source varchar(64),
    user_id int,
    user_name varchar(15),
    user_screen_name varchar(50),
    user_description varchar(160),
    user_followers_count int,
    user_location varchar(280),
    lat decimal(5,2),
    lon decimal(5,2),

    PRIMARY KEY (tweet_id)
);

-- load data from hashtag_joebiden.csv into temporary table
SELECT 'Load data from hashtag_joebiden.csv into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/05-2020-Election/hashtag_joebiden.csv'
IGNORE INTO TABLE TweetBidenRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1000000 LINES
(created_at,tweet_id,tweet,likes,retweet_count,source,user_id,user_name,user_screen_name,user_description,
@dummy,user_followers_count,user_location,lat,lon,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy)
SET hashtag_biden = b'1';

-- load data from hashtag_donaldtrump.csv into temporary table
SELECT 'Load data from hashtag_donaldtrump.csv into temporary table' AS '';
LOAD DATA INFILE '/var/lib/mysql-files/05-2020-Election/hashtag_donaldtrump.csv'
IGNORE INTO TABLE TweetTrumpRaw
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1200000 LINES
(created_at,tweet_id,tweet,likes,retweet_count,source,user_id,user_name,user_screen_name,user_description,
@dummy,user_followers_count,user_location,lat,lon,@dummy,@dummy,@dummy,@dummy,@dummy,@dummy)
SET hashtag_trump = b'1';

-- insert data from TweetBidenRaw and TweetTrumpRaw temporary table into TweetBidenTrumpRaw temporary table
SELECT 'Insert data from temporary table into temporary table' AS '';
INSERT INTO TweetBidenTrumpRaw
    SELECT 
        TweetBidenRaw.tweet_id,
        TweetBidenRaw.hashtag_biden,
        TweetBidenRaw.hashtag_trump,
        TweetBidenRaw.created_at,
        TweetBidenRaw.tweet,
        TweetBidenRaw.likes,
        TweetBidenRaw.retweet_count,
        TweetBidenRaw.source,
        TweetBidenRaw.user_id,
        TweetBidenRaw.user_name,
        TweetBidenRaw.user_screen_name,
        TweetBidenRaw.user_description,
        TweetBidenRaw.user_followers_count,
        TweetBidenRaw.user_location,
        TweetBidenRaw.lat,
        TweetBidenRaw.lon
    FROM TweetBidenRaw INNER JOIN TweetTrumpRaw USING (tweet_id);
UPDATE TweetBidenTrumpRaw SET hashtag_biden = b'1';
UPDATE TweetBidenTrumpRaw SET hashtag_trump = b'1';

-- create Tweet table
SELECT 'Create Tweet table' AS '';
CREATE TABLE Tweet (
    tweet_id decimal(65, 30),
    hashtag_biden bit DEFAULT b'0',
    hashtag_trump bit DEFAULT b'0',
    created_at datetime,
    tweet varchar(280),
    likes int,
    retweet_count int,
    source varchar(64),
    user_id int,
    user_name varchar(15),
    user_screen_name varchar(50),
    user_description varchar(160),
    user_followers_count int,
    user_location varchar(280),
    lat decimal(5,2),
    lon decimal(5,2),

    PRIMARY KEY (tweet_id),
    FOREIGN KEY (lat, lon) REFERENCES Location (lat, lon)
        ON UPDATE CASCADE 
        ON DELETE CASCADE
);

-- insert data from TweetBidenTrumpRaw temporary table into Tweet table 
SELECT 'Insert data from temporary table into Tweet table' AS '';
INSERT INTO Tweet
    SELECT 
        tweet_id,
        hashtag_biden,
        hashtag_trump,
        created_at,
        tweet,
        likes,
        retweet_count,
        source,
        user_id,
        user_name,
        user_screen_name,
        user_description,
        user_followers_count,
        user_location,
        lat,
        lon
    FROM TweetBidenTrumpRaw INNER JOIN Location USING (lon, lat);

-- insert data from TweetBidenRaw temporary table into Tweet table 
SELECT 'Insert data from temporary table into Tweet table' AS '';
INSERT INTO Tweet
    SELECT
        tweet_id,
        hashtag_biden,
        hashtag_trump,
        created_at,
        tweet,
        likes,
        retweet_count,
        source,
        user_id,
        user_name,
        user_screen_name,
        user_description,
        user_followers_count,
        user_location,
        lat,
        lon
    FROM TweetBidenRaw INNER JOIN Location USING (lon, lat)
    WHERE (tweet_id) NOT IN
        (SELECT tweet_id from TweetBidenTrumpRaw);

-- insert data from TweetTrumpRaw temporary table into Tweet table 
SELECT 'Insert data from temporary table into Tweet table' AS '';
INSERT INTO Tweet
    SELECT
        tweet_id,
        hashtag_biden,
        hashtag_trump,
        created_at,
        tweet,
        likes,
        retweet_count,
        source,
        user_id,
        user_name,
        user_screen_name,
        user_description,
        user_followers_count,
        user_location,
        lat,
        lon
    FROM TweetTrumpRaw INNER JOIN Location USING (lon, lat)
    WHERE (tweet_id) NOT IN
        (SELECT tweet_id from TweetBidenTrumpRaw);

-- drop TweetBidenRaw
SELECT 'Drop temporary table' AS '';
DROP TABLE TweetBidenRaw;
-- drop TweetTrumpRaw
SELECT 'Drop temporary table' AS '';
DROP TABLE TweetTrumpRaw;
-- drop TweetBidenTrumpRaw
SELECT 'Drop temporary table' AS '';
DROP TABLE TweetBidenTrumpRaw;


-- ****************************************************************************
-- Indexes
-- ****************************************************************************
SELECT '----------------------------------------------------------------' AS '';

-- create index on CountyPresidentCandidate on the column total_votes 
SELECT 'Create index on CountyPresidentCandidate on the column total_votes' AS '';
CREATE INDEX idx_tV ON CountyPresidentCandidate (total_votes);

-- create index on StatePoll on the column display_name 
SELECT 'Create index on StatePoll on the column display_name' AS '';
CREATE INDEX idx_dN ON StatePoll (display_name);

-- create index on StatePoll on the column state_name 
SELECT 'Create index on StatePoll on the column state_name' AS '';
CREATE INDEX idx_sN ON StatePoll (state_name);

-- create index on Tweet on the columns hashtag_biden, hashtag_trump
SELECT 'Create index on Tweet on the columns hashtag_biden, hashtag_trump' AS '';
CREATE INDEX idx_hB_bT ON Tweet (hashtag_biden, hashtag_trump);

-- create index on Tweet on the columns hashtag_biden, hashtag_trump, source
SELECT 'Create index on Tweet on the columns hashtag_biden, hashtag_trump, source' AS '';
CREATE INDEX idx_hB_bT_s ON Tweet (hashtag_biden, hashtag_trump, source);
