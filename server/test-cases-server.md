# 2020 Election Databaser Server Test Cases

The following three tests are used to verify that the database setup file
election2020-database-server.sql was run correctly and successfully 
prepared the database for use with the client. These tests should be run 
after running the SQL script. While the tests are not exhausive, they
cover the most important aspects of the database setup.

## Test 1
#### Purpose
The purpose of this test is to ensure that all tables for the database were
created.
#### Input
    SHOW TABLES;
#### Expected Output
A list of tables which includes the following should be displayed:

    Candidate                
    County                   
    CountyPresidentCandidate 
    CountyStatistics         
    CountyZipcode            
    Location                 
    State                    
    StatePoll               
    Tweet      

## Test 2
#### Purpose
The purpose of this test is to ensure that all tables for the database were
populated.
#### Input
    SELECT count(*) FROM <table_name>
where <table_name> is each table from the database which can be seen by running

    SHOW TABLES;
#### Expected Output
The expected output is that all tables are populated, that is no tables have a row
count of 0.

## Test 3
#### Purpose
The purpose of this test is to check that all indexes were created.
#### Input
    SHOW INDEX FROM <table_name>;
where <table_name> is each of the following tables:

    CountyPresidentCandidate
    StatePoll
    Tweet
#### Expected Output
The expected output is the follwing indexes exist on each of those three tables:

    CountyPresidentCandidate
        idx_tV
    StatePoll
        idx_dN
        idx_sN
    Tweet
        idx_hB_bT
        idx_hB_bT_s
